package ua.kiev.tsv.srv;


import java.net.InetSocketAddress;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import io.netty.util.concurrent.ImmediateEventExecutor;
import ua.kiev.tsv.srv.dao.Dao;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaderNames.LOCATION;
import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static io.netty.handler.codec.http.HttpResponseStatus.FOUND;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

class ServerHandler extends SimpleChannelInboundHandler<Object> {
  public static final String REDIRECT_URL = "^/redirect[?]url=.*$";
  static DefaultChannelGroup allChannels = new DefaultChannelGroup(ImmediateEventExecutor.INSTANCE);
  private static Timer timer = new HashedWheelTimer();
  private ChannelRequest cr;

  ServerHandler(ChannelRequest cr) {
    this.cr = cr;
  }

  private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) {
    if (res.status().code() != OK.code()) {
      ByteBuf buf = Unpooled.copiedBuffer(res.status().toString(), CharsetUtil.UTF_8);
      res.content().writeBytes(buf);
      buf.release();
      HttpHeaderUtil.setContentLength(res, res.content().readableBytes());
    }

    ChannelFuture f = ctx.channel().writeAndFlush(res);
    if (!HttpHeaderUtil.isKeepAlive(req) || res.status().code() != OK.code()) {
      f.addListener(ChannelFutureListener.CLOSE);
      ctx.close();
    }
  }

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) {
    ctx.flush();
  }

  @Override
  public void messageReceived(ChannelHandlerContext ctx, Object msg) {
    if (msg instanceof FullHttpRequest) {
      allChannels.add(ctx.channel());
      handleHttpRequest(ctx, (FullHttpRequest) msg);
    }
  }

  private void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest req) {
    // bad request
    if (!req.decoderResult().isSuccess()) {
      sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST));
      return;
    }

    if (req.method() != GET) {
      sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN));
      return;
    }

    if ("/hello".equals(req.uri())) {
      ByteBuf content = ServerIndexPage.getHelloContent();
      FullHttpResponse res = new DefaultFullHttpResponse(HTTP_1_1, OK, content);

      String ip = ((InetSocketAddress) (ctx.channel()).remoteAddress()).getAddress().getHostAddress();
      Dao.getInstance().addServerRequest(ip);

      res.headers().set(CONTENT_TYPE, "text/html; charset=UTF-8");
      HttpHeaderUtil.setContentLength(res, content.readableBytes());
      cr.addUrl(req.uri());
      timer.newTimeout(new HelloPageTimer(ctx, req, res), 10, TimeUnit.SECONDS);
      return;
    }

    if ("/status".equals(req.uri())) {
      ByteBuf content = ServerIndexPage.getStatusContent();
      FullHttpResponse res = new DefaultFullHttpResponse(HTTP_1_1, OK, content);

      String ip = ((InetSocketAddress) (ctx.channel()).remoteAddress()).getAddress().getHostAddress();
      Dao.getInstance().addServerRequest(ip);

      res.headers().set(CONTENT_TYPE, "text/html; charset=UTF-8");
      HttpHeaderUtil.setContentLength(res, content.readableBytes());
      cr.addUrl(req.uri());
      sendHttpResponse(ctx, req, res);
      return;
    }
    Pattern redirect = Pattern.compile(REDIRECT_URL);
    Matcher matcher = redirect.matcher(req.uri());
    if (matcher.matches()) {
      QueryStringDecoder qsd = new QueryStringDecoder(req.uri());
      List<String> url = qsd.parameters().get("url");

      if (!Objects.equals(url.get(0), "")) {
        Dao.getInstance().addRedirectRequest(url.get(0));
      }
      String ip = ((InetSocketAddress) (ctx.channel()).remoteAddress()).getAddress().getHostAddress();
      Dao.getInstance().addServerRequest(ip);

      FullHttpResponse res = new DefaultFullHttpResponse(HTTP_1_1, FOUND);
      res.headers().set(LOCATION, "http://" + url.get(0));
      cr.addUrl(url.get(0));
      sendHttpResponse(ctx, req, res);
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    ctx.close();
  }

  private static class HelloPageTimer implements TimerTask {
    ChannelHandlerContext ctx;
    FullHttpRequest req;
    FullHttpResponse res;


    HelloPageTimer(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) {
      this.ctx = ctx;
      this.req = req;
      this.res = res;
    }

    @Override
    public void run(Timeout timeout) throws Exception {
      sendHttpResponse(ctx, req, res);
    }
  }

}
