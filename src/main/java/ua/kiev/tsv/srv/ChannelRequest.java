package ua.kiev.tsv.srv;

import java.time.LocalDateTime;

public class ChannelRequest {


  private String ip;
  private LocalDateTime timeStart;
  private String[] url;

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public LocalDateTime getTimeStart() {
    return timeStart;
  }

  public void setTimeStart(LocalDateTime timeStart) {
    this.timeStart = timeStart;
  }

  public String[] getUrl() {
    return this.url;
  }

  public void addUrl(String uri) {

  }
}
