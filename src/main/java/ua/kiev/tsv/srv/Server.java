package ua.kiev.tsv.srv;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ua.kiev.tsv.srv.dao.Dao;

public class Server {

  private static final int PORT = 8080;

  public static void main(String[] args) throws Exception {
    InternalLoggerFactory.setDefaultFactory(new Slf4JLoggerFactory());


    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    try {
      ServerBootstrap b = new ServerBootstrap();
      b.group(bossGroup, workerGroup)
       .channel(NioServerSocketChannel.class)
//       .handler(new LoggingHandler(LogLevel.INFO))
       .childHandler(new ServerInitializer());
      Channel ch = b.bind(PORT).sync().channel();
      System.out.println(" >> http://127.0.0.1:" + PORT + "/status");
      System.out.println(" >> http://127.0.0.1:" + PORT + "/hello -- hello page after 10 seconds delay");
      System.out.println(" >> http://127.0.0.1:" + PORT + "/redirect?url=YOUR_URL_HERE to redirect to desired page");
      ch.closeFuture().sync();
    } finally {
      bossGroup.shutdownGracefully();
      workerGroup.shutdownGracefully();
      Dao.getInstance().stop();
    }
  }


}
