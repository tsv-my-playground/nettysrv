package ua.kiev.tsv.srv.dao;

import com.j256.ormlite.table.DatabaseTable;

/**
 * @author sv.tsimbalyuk@gmail.com
 */

@DatabaseTable(tableName = "request_redirect")
public class RequestRedirect {
  public static final String FIELD_ID = "id";
  public static final String FIELD_URL = "url";
  public static final String FIELD_COUNT = "count";

  private int id;
  private String url;
  private int count;

  public RequestRedirect(int id, String url, int count) {
    this.id = id;
    this.url = url;
    this.count = count;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
