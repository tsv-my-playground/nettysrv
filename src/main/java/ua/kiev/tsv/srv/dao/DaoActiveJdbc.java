package ua.kiev.tsv.srv.dao;

import org.javalite.activejdbc.Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DaoActiveJdbc {
  private static final Logger log = LoggerFactory.getLogger(DaoActiveJdbc.class);

  static {
    Base.open();
  }
}
