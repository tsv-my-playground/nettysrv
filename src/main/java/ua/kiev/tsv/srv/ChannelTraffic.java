package ua.kiev.tsv.srv;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import ua.kiev.tsv.srv.dao.ChannelView;
import ua.kiev.tsv.srv.dao.Dao;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

class ChannelTraffic extends ChannelTrafficShapingHandler {

  private ChannelRequest cr;

  ChannelTraffic(long checkInterval, ChannelRequest cr) {
    super(checkInterval);
    this.cr = cr;
  }


  @Override
  public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
    super.handlerAdded(ctx);
    this.trafficCounter().start();
  }

  @Override
  public synchronized void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    super.handlerRemoved(ctx);
    this.trafficCounter().stop();
    LocalDateTime timeEnd = LocalDateTime.now();

    double connectionDuration = ChronoUnit.SECONDS.between(cr.getTimeStart(), timeEnd);
    long sentBytes = this.trafficCounter().cumulativeReadBytes();
    long receivedBytes = this.trafficCounter().cumulativeWrittenBytes();
    double speed = (sentBytes + receivedBytes) / connectionDuration;

    StringBuilder url = new StringBuilder();

    if (cr.getUrl() != null) {
      for (String str : cr.getUrl()) {
        if (!str.equals(" ")) {
          url.append(str).append(" ");
        } else {
          url = new StringBuilder("http://127.0.0.1/");
        }
      }
    } else {
      url = new StringBuilder("http://127.0.0.1/");
    }
    Dao.getInstance().addChannelView(new ChannelView(cr.getIp(), url.toString(), cr.getTimeStart().toString(),
        timeEnd.toString(), sentBytes, receivedBytes, speed));
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    super.exceptionCaught(ctx, cause);
    cause.printStackTrace();
  }
}
