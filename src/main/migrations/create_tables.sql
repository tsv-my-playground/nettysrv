CREATE TABLE request_redirect(
  id    INT NOT NULL AUTO_INCREMENT,
  url   VARCHAR(45),
  count INT,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE request_server(
  id    INT NOT NULL AUTO_INCREMENT,
  ip    VARCHAR(45),
  count INT,
  last  VARCHAR(45),
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE request_channel(
  id             INT NOT NULL AUTO_INCREMENT,
  ip             VARCHAR(45),
  url            VARCHAR(45),
  time_start     VARCHAR(45),
  time_end       VARCHAR(45),
  sent_bytes     INT,
  received_bytes INT,
  speed          DOUBLE,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;