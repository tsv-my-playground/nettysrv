#!/bin/bash

# https://github.com/jbergstroem/mariadb-alpine
#docker run --rm \
docker run \
	--name=db \
	--net=host \
	-p 3306:3306 \
        -v db:/var/lib/mysql \
        -e MYSQL_ROOT_PASSWORD=root \
        -e SKIP_INNODB=yes \
        -e MYSQL_USER=test \
        -e MYSQL_DATABASE=test \
        -e MYSQL_PASSWORD=test \
         jbergstroem/mariadb-alpine
